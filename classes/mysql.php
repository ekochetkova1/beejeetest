<?
/** .......................................................
@desc SQL - класс работы с MySQL
**/
class sql{
	var $host;			// Host
	var $user;			// Database user
	var $pass;			// Database password
	var $dbname;		// Database name
	var $dbconnect;	// Database connection link identifier
	var $result;		// Last returned result identifier
	protected static $_instance;
/** .......................................................
@return bool
@desc Class constructor
**/
	private function __construct($host,$user,$pass){
		$this->host=$host;
		$this->user=$user;
		$this->pass=$pass;
		
		//$this->dbconnect = mysql_connect($this->host,$this->user,$this->pass);
		$this->dbconnect =  new mysqli($this->host, $this->user, $this->pass, "ekochetkova1");
if (mysqli_connect_errno()) {
	//echo '777'; exit;
    printf("Ошибка соединения: %s\n", mysqli_connect_error());
    exit();
}

mysqli_query($this->dbconnect, "SET character_set_client = '".DB_LOCAL."'");
		mysqli_query($this->dbconnect, "SET character_set_connection = '".DB_LOCAL."'");
		mysqli_query($this->dbconnect, "SET character_set_results = '".DB_LOCAL."'");  
		mysqli_query($this->dbconnect, "SET NAMES '".DB_LOCAL."'"); 
mysqli_set_charset ($this->dbconnect, DB_CHARSET);				
		//var_dump($this->dbconnect);
		if(!$this->dbconnect){
			if(DEBUG){ echo "<font color=#cccccc>sql error: ".mysql_errno().": ". mysql_error()."</font>\n";}
			return false;
		}else{
			return true;
		}
		
		
	}
	
	 public static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self(MYSQL_HOSTNAME,MYSQL_DBUSER,MYSQL_DBPASS);  
			self::$_instance->set_db(MYSQL_DATABASE);	
			//self::set_locale(DB_LOCAL);	
			//self::set_charset(DB_CHARSET);		
        } 
        return self::$_instance;
    }
/** .......................................................
@return void
@desc Class destructor
**/
	function disconnect(){
		@mysql_close($this->dbconnect);
		$this->dbconnect = false;
	}

/** .......................................................*/
//@return bool
//@desc Select a database locale
	public static function set_locale($locale){
		/*var_dump($this->dbconnect );
		mysqli_query($this->dbconnect, "SET character_set_client = '".$locale."'");
		mysqli_query($this->dbconnect, "SET character_set_connection = '".$locale."'");
		mysqli_query($this->dbconnect, "SET character_set_results = '".$locale."'");  
		mysqli_query($this->dbconnect, "SET NAMES '".$locale."'");  */
	}

	public static function set_charset($charset){
		mysqli_set_charset ($charset);		
	}

/** .......................................................
@return bool
@desc Select a MySQL database
**/
	public function set_db($dbname){
		$db = @mysqli_select_db($dbname);
		if($db){
			$this->dbname = $dbname;
			return true;
		}else{
		//	if(DEBUG){ echo "<font color=#cccccc>sql error: ".mysql_errno().": ". mysql_error()."</font>\n";}
			return false;
		}
	}
/** .......................................................
@return int
@desc Get number of rows in result
**/
	function num($result=0){
		if($result==0) $result = $this->result;
		return 	@mysql_num_rows($result);
	}
/** .......................................................
@return resource
@desc Send a MySQL query
**/
	function send($query,$dbname=""){
		if($dbname==""){
			$this->result = @mysqli_query($this->dbconnect, $query);
		}else{
			$this->result = @mysql_db_query($dbname,$query);
		}
		if(!$this->result){
			//if(DEBUG){ echo "<font color=#cccccc>sql error: ".mysql_errno().": ". mysql_error()."<br />".$query."<br /><br /><br /></font>\n";}
		}
		return $this->result;
	}
/** .......................................................
@return mixed
@desc Get a next result row as an enumerated array
**/
	function get($result=0){
	//	if($result==0) $result = $this->result;
		$row = @mysqli_fetch_assoc($result);
		if(!is_array($row)){
			return false;
		}else
			return $row;
	}
/** .......................................................
@return void
@desc Free result memory
**/
	function free($result=0){
		if($result==0){
			@mysql_free_result($this->result);
			$this->result = 0;
		}else{
			@mysql_free_result($result);
		}
	}

/** .......................................................
@return int
@desc возвращает количество строк обработанных запросом типа DELETE, UPDATE
**/
   function affect($result=0){
      return @mysql_affected_rows (($result==0)?$this->result:$result);
   }

/** .......................................................
@return int
@desc Get the id generated from the previous INSERT operation
**/
	function get_id(){
		if($id = @mysql_insert_id()) return $id;
		else												 return 0;
	}
/** .......................................................
@return void
@desc Locks tables for the current thread
**/
	function lock($tables,$dbname=""){
		$tables_ar = explode(",",$tables);
		if(sizeof($tables_ar)>1)	$tables = implode(" WRITE, ",$tables_ar)." WRITE";
		$query = "LOCK TABLES ".$tables." WRITE";
		$result = $this->result;
		if($this->send($query,$dbname)) $this->free();
		$this->result = $result;
	}
/** .......................................................
@return void
@desc Releases any locks held by the current thread
**/
	function unlock($dbname=""){
		$query = "UNLOCK TABLES";
		$result = $this->result;
		if($this->send($query,$dbname)) $this->free();
		$this->result = $result;
	}


}
?>