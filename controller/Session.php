<?
class Session{
	//синглтон
	
	var $session;
	protected static $_instance;
	
	private function __construct(){
		$this->session = $_SESSION;		
		return true;	
	}
	
	public static function getInstance() {       
		if (self::$_instance === null) {
            self::$_instance = new self();  			
        }
		return self::$_instance;
	}
	
	public function GetSessionArray($arr_name) {		
		if(isset($_SESSION[$arr_name])){
			return $_SESSION[$arr_name];
		}
		else{
			return [];
		}	
    }
	public function SetSessionArray($arr_name, $arr_data) {		
		$_SESSION[$arr_name] = $arr_data;		
    }
	
	public function SetSessionVar($var_name, $var_data){
		$_SESSION[$var_name] = $var_data;
	}
			
	public function GetSessionVar($var_name) {				
		if(isset($_SESSION[$var_name])){
			return $_SESSION[$var_name];
		}
		else{
			return "";
		}	
    }
	
	public function UnsetSessionVar($var_name){
		if(isset($_SESSION[$var_name])){
			unset($_SESSION[$var_name]);
		}
	}
	public function UnsetSessionArray($arr_name){
		if(isset($_SESSION[$arr_name])){			
			unset($_SESSION[$arr_name]);
		}
	}
}

?>