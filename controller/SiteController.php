<?
error_reporting(E_ALL);

include_once ("classes/db.php");
include_once ("classes/mysql.php");
include_once ("Session.php");
include_once ("templ.php");
include_once ("TasksController.php");
include_once ("UsersController.php");


class SiteController{
	
	var $getData;
	var $postData;
	var $session;
	var $url;
	var $url_arr;
	var $txt;
	var $template;
	
	
	public function __construct(){
		$this->session = session::getInstance();
		$this->template = new template();
		
		
		
		if(isset($_GET) && sizeof($_GET)>0){
			$this->getData = $_GET;			
		}
		if(isset($_POST) && sizeof($_POST)>0){
			$this->postData = $_POST;	
			$this->postData = array_map(function($var){
				return htmlspecialchars($var, ENT_QUOTES);
				}, $this->postData);
		}			
		
	
	    if(isset($_GET["url"]) && strlen($_GET["url"])>0){
			$this->url = $_GET["url"];
		}
		else{
			$this->url = "";
		}
	
		
		if (strlen($this->url)>0){
			
		  $this->url_arr = explode('/', rtrim($this->url, '/'));
		  
		}
		else{
		  $this->url_arr= null;
		}
		
	}
	
	 public function run() {	 	
			
			$this->txt.=	$this->DisplayTop();			
			$this->txt.=	$this->DisplayAlert();			
			$this->txt.=		$this->ParseGetData();			
			$this->txt.=		$this->DisplayBottom();		
		
		echo $this->txt;
    }
	
		
	private function DisplayBottom(){
	
		$this->template->get_tpl("view/SiteBottomView.php");
			
		$this->template->set_value('CURR_URL',CURR_URL);
		$this->template->tpl_parse();
		return $this->template->html;
		
	}
	
	
	
	public function DisplayTop(){
		$this->template->get_tpl("view/SiteHeaderView.php");
		
		$this->template->set_value('CURR_URL',CURR_URL);
		
		if($this->session->GetSessionVar('token')==""){
			
			$this->template->set_value('ACTION', 'login');
			$this->template->set_value('ACTION_TITLE', 'Войти');
		}
		else{
			
			$this->template->set_value('ACTION', 'logout');
			$this->template->set_value('ACTION_TITLE', 'Выход');
		}
		
		$this->template->tpl_parse();
		return $this->template->html;
		
	}
	
	public function DisplayAlert(){
		
		
		$alertTxt = $this->session->GetSessionVar('alert');
		
		if($alertTxt != ""){
			
			
			$this->template->get_tpl("view/AlertView.php");
			$this->template->set_value('ALERT_TEXT', $alertTxt);
			$this->template->set_value('ALERT_TYPE', $this->session->GetSessionVar('alertType'));
			
			$this->session->UnsetSessionVar('alert');
			$this->session->UnsetSessionVar('alertType');
		
			$this->template->tpl_parse();
			return $this->template->html;
		}
		
		return "";
		
		
	}
	
	public function DisplayMainPage($page=1){
		
		
				$this->template->get_tpl("view/MainPageView.php");
		
				$this->template->set_value('CURR_URL',CURR_URL);
				
				$submittedData = $this->session->GetSessionArray('submittedData');
				if(sizeof($submittedData)>0){
					$this->template->set_value('EMAIL',$submittedData['email']);
					$this->template->set_value('NAME',$submittedData['name']);
					$this->template->set_value('DESCRIPTION',$submittedData['description']);
					$this->session->UnsetSessionArray('submittedData');
				}
				else{
					$this->template->set_value('EMAIL', '');
					$this->template->set_value('NAME', '');
					$this->template->set_value('DESCRIPTION', '');
				}
				
				$tasks = new TasksController();
				$tasksHtml = $tasks->getTasks($page, $this->getData);
				$paginationHtms = $tasks->getPagination($page, $this->getData);
				$this->template->set_value('TASKS_LIST', $tasksHtml );
				$this->template->set_value('PAGINATION', $paginationHtms );
				
				
				
				$this->template->set_value('CURR_PAGE', $page );
				
								
				if(isset($this->getData["order"]) && $this->getData["sort"]){
					switch($this->getData['order']){
						case 'name':
							$this->getData["sort"]=='ASC'?$this->template->set_value('NAME_SORT_TYPE', 'DESC'):$this->template->set_value('NAME_SORT_TYPE', 'ASC');
							$this->template->set_value('EMAIL_SORT_TYPE', 'ASC');
							$this->template->set_value('STATUS_SORT_TYPE', 'ASC');
							
						break;
						case 'email':
							$this->getData["sort"]=='ASC'?$this->template->set_value('EMAIL_SORT_TYPE', 'DESC'):$this->template->set_value('EMAIL_SORT_TYPE', 'ASC');
							$this->template->set_value('NAME_SORT_TYPE', 'ASC');
							$this->template->set_value('STATUS_SORT_TYPE', 'ASC');
							
						break;
						case 'title':
							$this->getData["sort"]=='ASC'?$this->template->set_value('STATUS_SORT_TYPE', 'DESC'):$this->template->set_value('STATUS_SORT_TYPE', 'ASC');
							$this->template->set_value('EMAIL_SORT_TYPE', 'ASC');
							$this->template->set_value('NAME_SORT_TYPE', 'ASC');
						break;
						default:
							$this->template->set_value('EMAIL_SORT_TYPE', 'ASC');
							$this->template->set_value('NAME_SORT_TYPE', 'ASC');
							$this->template->set_value('STATUS_SORT_TYPE', 'ASC');
						
					}
					
				}
				else{
						$this->template->set_value('EMAIL_SORT_TYPE', 'ASC');
						$this->template->set_value('NAME_SORT_TYPE', 'ASC');
						$this->template->set_value('STATUS_SORT_TYPE', 'ASC');
				}
				
				$this->template->tpl_parse();
				return $this->template->html;
	}
	
	
	private function ParseGetData(){
			$page_content = "";
			
			if(isset($this->url_arr[0])){			 
				switch ($this->url_arr[0]){
					case "tasks":
						
						$tasks = new TasksController();
						return $tasks->ParseUrlAction($this->url_arr, $this->postData);
					break;
					case "task":
						$taskId = (isset($this->url_arr[1]))?$this->url_arr[1]:0;
						$tasks = new TasksController();
						return $tasks->getTaskEditForm($taskId);
					break;
					case "taskUpdate":						
						$tasks = new TasksController();
						return $tasks->UpdateTask($this->postData);
					break;
					case "page":
						$page = (isset($this->url_arr[1]))?$this->url_arr[1]:1;
						
						return $this->DisplayMainPage($page);
					break;
					case "login":
						$user = new UsersController();
						return $user->GetLoginForm();
					break;
					case "logout":
						$user = new UsersController();
						return $user->Logout();
					break;
					case "autorization":
						if(isset($this->postData['login']) && isset($this->postData['password'])){
							$user = new UsersController();
							return $user->autorization($this->postData['login'], $this->postData['password']);
						}
						else{
							$this->session->SetSessionVar('alert', "Не указаны реквизиты доступа");
							$this->session->SetSessionVar('alertType', 'danger');
							$this->session->SetSessionArray('submittedData', $this->data);
							header("Location: ".CURR_URL."login");
						}
					break;
					
					default:
					//	some 404 error
					break;
				}							
			}
			else{
				return $this->DisplayMainPage();
				
				
			}
			
		
	}
	
	

	
	
}
?>