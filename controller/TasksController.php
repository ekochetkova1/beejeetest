<?
include_once ("model/TasksModel.php");
class TasksController {
	
	var $nModel;	
	var $nId;
	var $data;
	

	
	public function __construct(){
		$this->session = session::getInstance();	
		$this->nModel = new TasksModel();
		$this->template = new template();	
		
		return true;	
	}
	
	public function ParseUrlAction($url=NULL, $postData = NULL){
		if(!isset($url)){
			return false;
		}
		
		if(isset($url[1])){
			switch ($url[1]){
					case "submit":
						$this->data = $postData;
						$this->createTask();
					break;
					
					default:
					//	здесь какая-нибудь обрабтка 404
					break;
			}
		}
	
		
	}
	
	
	public function getTaskEditForm($taskId){
		$user = new UsersController();
		$taskHtml = "";
		if($user->CheckUser()){
			$task = $this->nModel->GetTaskById($taskId);
			
			if(sizeof($task)>0){
				
				$this->template->get_tpl("view/TaskEditView.php");
				
				$this->template->set_value('CURR_URL',CURR_URL);
				$this->template->set_value('ID',$task['id']);
				$this->template->set_value('NAME',$task['name']);
				$this->template->set_value('EMAIL',$task['email']);
				$this->template->set_value('DESCRIPTION',$task['description']);
			//	$this->template->set_value('STATUS',$task['title']);
			
				if($task['statusId']==2){
					$this->template->set_value('CHECKED','checked');
				}
				else{
					$this->template->set_value('CHECKED','');
				}
					
				$this->template->tpl_parse();
				$taskHtml = $this->template->html;
			}
			else{
				$taskHtml = "Такой задачи не существует";
			}
			
		}
		else{
			$taskHtml = "У вас нет прав для редактирования задачи";
		}
		
		return $taskHtml;
	}
	
	public function UpdateTask($data){
		
		$user = new UsersController();
		$taskHtml = "";
		if($user->CheckUser()){
			
			$task = $this->nModel->GetTaskById($data['id']);
			
			if(sizeof($task)>0){
				$edited = ($task['description']==$data['description'])?0:1;

				$statusId = (isset($data['status']))?2:1;
				$this->nModel->UpdateTask($data['id'], $data['description'], $edited, $statusId);
				
				$this->session->SetSessionVar('alert', 'Задача обновлена');
				$this->session->SetSessionVar('alertType', 'success');
				header("Location: ".CURR_URL."task/".$data['id']);
			}
			else{
				$this->session->SetSessionVar('alert', 'Задача не существует');
				$this->session->SetSessionVar('alertType', 'danger');
				$this->session->SetSessionArray('submittedData', $this->data);
				header("Location: ".CURR_URL);
			}
			
		}
		else{
			$this->session->SetSessionVar('alert', 'У вас нет прав для изменения задач');
			$this->session->SetSessionVar('alertType', 'danger');
			$this->session->SetSessionArray('submittedData', $this->data);
			header("Location: ".CURR_URL);
		}
	}
	
	
	public function createTask(){
		$error = $this->validateData();
		
		if($error['code']==0){
			$this->nModel->newTask($this->data);
		
			$this->session->SetSessionVar('alert', 'Запись добавлена');
			$this->session->SetSessionVar('alertType', 'success');
			header("Location: ".CURR_URL);
		}
		else{
			$this->session->SetSessionVar('alert', $error['description']);
			$this->session->SetSessionVar('alertType', 'danger');
			$this->session->SetSessionArray('submittedData', $this->data);
			header("Location: ".CURR_URL);
		}
		
	}
	
	
	private function validateData(){
		$error = [];
		if($this->data['name']=="" || $this->data['email']=="" || $this->data['description']==""){
			$error['code'] = 1;
			$error['description'] = "Все поля должны быть заполнены";
			return $error;
			
		}
		
		if(!preg_match('#^([\w]+\.?)+(?<!\.)@(?!\.)[a-zа-я0-9ё\.-]+\.?[a-zа-яё]{2,}$#ui', $this->data['email'])){
			$error['code'] = 2;
			$error['description'] = "Указан некорректный email";
			return $error;
			
		}
		
		
		
		return ['code' => 0, 'description' => 'OK'];
		
	}
	
	
	
	public function getTasks($page, $orderParams=[]){
		
		$user = new UsersController();
		
		
		if($page > $this->getPagesCount()){
			return false;
		}
		
		$from = ($page-1) * TASKS_AT_PAGE;
		
		if(isset($orderParams['order']) && isset($orderParams['sort'])){
			$tasks = $this->nModel->getTasks($from, $orderParams['order'], $orderParams['sort']);
		}
		else{
			$tasks = $this->nModel->getTasks($from);
		}
		
		
		$tasksHtml = "";
		
		if(sizeof($tasks)>0){
			foreach($tasks as $key => $val){
				if($user->CheckUser()){
					$this->template->get_tpl("view/TaskAdminItemView.php");
				}
				else{
					$this->template->get_tpl("view/TaskItemView.php");
				}
				$this->template->set_value('CURR_URL',CURR_URL);
				$this->template->set_value('ID',$val['id']);
				$this->template->set_value('NAME',$val['name']);
				$this->template->set_value('EMAIL',$val['email']);
				$this->template->set_value('DESCRIPTION',$val['description']);
				$this->template->set_value('STATUS',$val['title']);
				($val['edited'])?$this->template->set_value('EDITED',"Отредактировано администратором: "):$this->template->set_value('EDITED',"");
				
				$this->template->tpl_parse();
				$tasksHtml.=$this->template->html;
			}
		}
		
		return $tasksHtml;
	}
	
	public function GetItemNumber($page){
		$tasksCount = $this->nModel->getTasksCount();
		if($tasksCount>0){
			$pagesCount = ceil($tasksCount / TASKS_AT_PAGE);
		}
		return 0;
	}
	
	public function getPagesCount(){
		$tasksCount = $this->nModel->getTasksCount();
		if($tasksCount>0){
			return ceil($tasksCount / TASKS_AT_PAGE);
		}
		return 0;
	}
	
	public function getPagination($page, $orderParms=[]){
		$pagesCount = $this->getPagesCount();
		
		$paginationHtml = "";
		for($i=1; $i<=$pagesCount; $i++){
			$this->template->get_tpl("view/PaginationItemView.php");
				$this->template->set_value('PAGE_NUM',$i);
				$this->template->set_value('CURR_URL', CURR_URL);
				
				$i==$page?$this->template->set_value('ACTIVE', 'active'):$this->template->set_value('ACTIVE', '');
				
				if(isset($orderParms["order"]) && isset($orderParms["sort"])){
					$this->template->set_value('ORDER_PARAMS', "?order=" . $orderParms["order"] . "&sort=" . $orderParms["sort"] );
				}
				else{
					$this->template->set_value('ORDER_PARAMS', "");
				}
								
				$this->template->tpl_parse();
				$paginationHtml.=$this->template->html;
		}
		return $paginationHtml;
	}
	
	
	
	
}



?>