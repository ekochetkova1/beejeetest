<?
include_once ("model/UsersModel.php");
class UsersController {
	
	var $nModel;	
	var $nId;
	var $data;

	
	public function __construct(){
		$this->session = session::getInstance();	
		$this->nModel = new UsersModel();
		$this->template = new template();	
		
		return true;	
	}
	
	
	public function GetLoginForm(){
		if($this->CheckUser()){
			header("Location: ".CURR_URL);
		}
		
		$this->template->get_tpl("view/LoginView.php");
		$submittedData = $this->session->GetSessionArray('submittedData');
				if(sizeof($submittedData)>0){
					$this->template->set_value('LOGIN',$submittedData['login']);					
					$this->template->set_value('PASSWORD',$submittedData['password']);					
				}
				else{
					$this->template->set_value('LOGIN', '');
					$this->template->set_value('PASSWORD', '');					
				}
		
		
		$this->template->tpl_parse();
		return $this->template->html;
		
	}
	
	public function Logout(){
		if(!$this->CheckUser()){
			header("Location: ".CURR_URL);
		}
		
		$user = $this->nModel->getUserByToken($this->session->GetSessionVar('token'));
		$this->nModel->setToken($user['id'], '');
		$this->session->UnsetSessionVar('token');				
		header("Location: ".CURR_URL);
		
	}
	
	public function autorization($login, $password){
		
		if($this->CheckUser()){
			header("Location: ".CURR_URL);
		}
		
		$error = $this->validateData($login, $password);
		
		if($error['code']==0){
			$userId = $this->nModel->GetUserByLoginAndPassord($login, $password);
			
			if($userId>0){
				
				$token = md5($userId.time());
				$this->nModel->setToken($userId, $token);
				$this->session->SetSessionVar('token', $token);				
				header("Location: ".CURR_URL);
			}
			else{
				
				$this->session->SetSessionVar('alert', "Некорректные реквизиты доступа");
				$this->session->SetSessionVar('alertType', 'danger');
				$this->session->SetSessionArray('submittedData', $this->data);
				header("Location: ".CURR_URL."login");
			}
			
		}
		else{
			$this->session->SetSessionVar('alert', $error['description']);
			$this->session->SetSessionVar('alertType', 'danger');
			$this->session->SetSessionArray('submittedData', $this->data);
			header("Location: ".CURR_URL."login");
		}
		
	}
	
	
	private function validateData($login, $password){
		$error = [];
		if($login=="" || $password=="" ){
			$error['code'] = 1;
			$error['description'] = "Все поля должны быть заполнены";
			return $error;
			
		}		
		
		return ['code' => 0, 'description' => 'OK'];
		
	}
	
	
	public function CheckUser(){
		
		
		$currentToken = $this->session->GetSessionVar('token');
		
		if($currentToken==""){
			return false;
		}
		
		$user = $this->nModel->getUserByToken($currentToken);		
	
		if(sizeof($user)>0){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	
	public function getUsers($page, $orderParams=[]){
		
		
		if($page > $this->getPagesCount()){
			return false;
		}
		
		$from = ($page-1) * Users_AT_PAGE;
		
		if(isset($orderParams['order']) && isset($orderParams['sort'])){
			$Users = $this->nModel->getUsers($from, $orderParams['order'], $orderParams['sort']);
		}
		else{
			$Users = $this->nModel->getUsers($from);
		}
		
		
		$UsersHtml = "";
		
		if(sizeof($Users)>0){
			foreach($Users as $key => $val){
				$this->template->get_tpl("view/TaskItemView.php");
				$this->template->set_value('NAME',$val['name']);
				$this->template->set_value('EMAIL',$val['email']);
				$this->template->set_value('DESCRIPTION',$val['description']);
				$this->template->set_value('STATUS',$val['title']);
				
				$this->template->tpl_parse();
				$UsersHtml.=$this->template->html;
			}
		}
		
		return $UsersHtml;
	}
	
	public function GetItemNumber($page){
		$UsersCount = $this->nModel->getUsersCount();
		if($UsersCount>0){
			$pagesCount = ceil($UsersCount / Users_AT_PAGE);
		}
		return 0;
	}
	
	public function getPagesCount(){
		$UsersCount = $this->nModel->getUsersCount();
		if($UsersCount>0){
			return ceil($UsersCount / Users_AT_PAGE);
		}
		return 0;
	}
	
	public function getPagination($page, $orderParms=[]){
		$pagesCount = $this->getPagesCount();
		
		$paginationHtml = "";
		for($i=1; $i<=$pagesCount; $i++){
			$this->template->get_tpl("view/PaginationItemView.php");
				$this->template->set_value('PAGE_NUM',$i);
				$this->template->set_value('CURR_URL', CURR_URL);
				
				$i==$page?$this->template->set_value('ACTIVE', 'active'):$this->template->set_value('ACTIVE', '');
				
				if(isset($orderParms["order"]) && isset($orderParms["sort"])){
					$this->template->set_value('ORDER_PARAMS', "?order=" . $orderParms["order"] . "&sort=" . $orderParms["sort"] );
				}
				else{
					$this->template->set_value('ORDER_PARAMS', "");
				}
								
				$this->template->tpl_parse();
				$paginationHtml.=$this->template->html;
		}
		return $paginationHtml;
	}
	
	
	
	
}



?>