<?

class template
{
	var $values=array();
	var $html;
	var $path='';
	function get_tpl($tpl_name)
	{
		$tpl_name=$this->path.$tpl_name;
		if (empty($tpl_name) || !file_exists($tpl_name))
		{
			return false;	
		}else{
			//$this->html=join('',file($tpl_name));	
			$this->html=file_get_contents ($tpl_name);	
		}
		
	}
	
	function set_value($key,$var)
	{
		$key='{{'.$key.'}}';
		//echo $key." '". $var."'<br>";
		$this->values[$key]=$var;	
	}
	
	function tpl_parse()
	{
		foreach($this->values as $find=>$replace)
		{
			//echo $find." ".$replace;
			$this->html=str_replace($find,$replace,$this->html);	
		}
		
	}
	
	
}
?>