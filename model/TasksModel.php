<?

class TasksModel{
	
	var $sql;
	
	public function __construct(){		
		$this->sql = sql::getInstance();		
		
		return true;	
	}
	
	
	public function GetTasks($from=0, $orderBy="", $orderType=""){
		$data = array();	

	
		$limit_txt = " LIMIT ".$from.", ".TASKS_AT_PAGE;
		
		$order_txt = "";
		if($orderBy!="" && $orderType!=""){
			$order_txt =  " ORDER BY " . $orderBy . " " . $orderType;
		}
			
		$query = "SELECT t.*, s.title FROM tasks t INNER JOIN statuses s on t.statusId=s.id" .$order_txt." ".$limit_txt;	
	
		$res = $this->sql->send($query);
		while($row = $this->sql->get($res)){
			$data[] = $row;
			
		}	
	
		return $data;
	}
	
	public function GetTaskById($id=0){
	
		$query = "SELECT t.*, s.title FROM tasks t INNER JOIN statuses s on t.statusId=s.id WHERE t.id=" . $id . " LIMIT 1";	
	
		$res = $this->sql->send($query);
		if($row = $this->sql->get($res)){
			return $row;
			
		}	
	
		return [];
	}
	
	public function GetTasksCount(){
		$query = "SELECT count(*) as cnt FROM tasks";	
	
		$res = $this->sql->send($query);
		if($row = $this->sql->get($res)){
			return $row['cnt'];
			
		}
		return 0;
	}
	
	public function newTask($data){
		
			
		$query = "INSERT INTO tasks (name, email, description, statusId, edited) VALUES ('" .$data['name']."', '". $data['email'] ."', '". $data['description'] ."', 1, 0)";	
	
		$this->sql->send($query);
		
		return 0;
	}
	
	public function UpdateTask($id, $description, $edited, $statusId){
					
		$query = "UPDATE tasks SET description='" . $description . "', statusId='".$statusId."', edited=".$edited." WHERE id=".$id;	
	
		$this->sql->send($query);
		
		return 0;
	}
	
	
	
	
}

?>