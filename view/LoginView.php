	<div class="col-md-4"></div>
	<div class="col-md-4">
		<h3>Авторизация</h3>
		<form action="/autorization" method="POST">
			  
			  <div class="form-group">
					<label for="name-input">Логин</label>
					<input type="input" class="form-control" id="login-input" name="login" required value="{{LOGIN}}" >
					
			  </div>
				<div class="form-group">
					<label for="password-input">Пароль</label>
					<input type="password" class="form-control" id="password-input" name="password" required value="{{PASSWORD}}" >
					
			  </div>
			
			  <button type="submit" class="btn btn-primary">Отправить</button>
		</form>
	</div>
<div class="col-md-4"></div>
