	<div class="col-md-12">
		<table class="table">
			<thead>
			 <tr>
					<th><a href="{{CURR_URL}}page/{{CURR_PAGE}}?order=name&sort={{NAME_SORT_TYPE}}">Имя пользователя</a>
					</th>
					<th><a href="{{CURR_URL}}page/{{CURR_PAGE}}?order=email&sort={{EMAIL_SORT_TYPE}}">Email</a>
					</th>
					<th><a href="#">Текст задачи</a>
					</th>
					<th><a href="{{CURR_URL}}page/{{CURR_PAGE}}?order=title&sort={{STATUS_SORT_TYPE}}">Статус</a>
					</th>
			</tr>
			</thead>
			<tbody>
			{{TASKS_LIST}}
			</tbody>
		</table>
	</div>
	<div class="col-md-12">
		<nav aria-label="Page navigation example">
		  <ul class="pagination">			
			{{PAGINATION}}		
		  </ul>
		</nav>
	</div>
	<div class="col-md-4">
		<h3>Создать задачу</h3>
		<form action="/tasks/submit" method="POST">
			  <div class="form-group">
					<label for="email-input">Email</label>
					<input type="email" class="form-control" id="email-input" name="email" aria-describedby="emailHelp" placeholder="name@example.com" value="{{EMAIL}}" required  >
					<small id="emailHelp" class="form-text text-muted">Ваш email</small>
			  </div>
			  
			  <div class="form-group">
					<label for="name-input">Имя</label>
					<input type="input" class="form-control" id="name-input" name="name" required value="{{NAME}}" >
					
			  </div>
			   
			   <div class="form-group">
					<label for="description-text">Описание</label>
					<textarea class="form-control" id="description-text" rows="5" name="description" required>{{DESCRIPTION}}</textarea>
			  </div>
			  <button type="submit" class="btn btn-primary">Отправить</button>
		</form>
	</div>

